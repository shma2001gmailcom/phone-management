#!/usr/bin/env bash

source ./common.sh

java=${JAVA_HOME}/bin/java
cd ../phone-java/build/libs || exit 1
${java} -cp phone-java.jar \
        -Dloader.main=org.misha.utils.ShellFacade \
        org.springframework.boot.loader.launch.PropertiesLauncher