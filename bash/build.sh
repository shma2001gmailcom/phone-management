#!/usr/bin/env bash

source ./common.sh

cd ../phone-java || exit 1
bash gradlew clean build
cp -r data  build/libs