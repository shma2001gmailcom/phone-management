#!/usr/bin/env bash

source ./common.sh
java=${JAVA_HOME}/bin/java

cd ../phone-java/build/libs || exit 1

${java} -jar phone-java.jar