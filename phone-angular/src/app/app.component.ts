import { Component } from '@angular/core';

@Component({
  selector: 'app-phones',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  tab = 0;
  title = 'Phone Management';
}
