import { ChangeDetectorRef, Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BookingReactiveService } from './booking-reactive.service';
import { PhoneReactiveService } from '../phone/phone-reactive.service';
import { TesterReactiveService } from '../tester/tester-reactive.service';
import { ReportsComponent } from '../report/reports.component';
import { Booking } from './booking';
import { Phone } from '../phone/phone';
import { Tester } from '../tester/tester';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookingComponent implements OnInit {
  @Input() booking: Booking;
  phoneArray: Set<Phone> = new Set<Phone>();
  testerArray: Tester[] = [];
  selectedPhone: '';
  selectedTester: '';
  phoneToReturnId;
  bookingReactiveService: BookingReactiveService;
  phoneReactiveService: PhoneReactiveService;
  testerReactiveService: TesterReactiveService;
  reportsComponent: ReportsComponent;

  constructor(
    bookingReactiveService: BookingReactiveService,
    phoneReactiveService: PhoneReactiveService,
    testerReactiveService: TesterReactiveService,
    reportsComponent: ReportsComponent,
    private cdr: ChangeDetectorRef
  ) {
    this.bookingReactiveService = bookingReactiveService;
    this.phoneReactiveService = phoneReactiveService;
    this.testerReactiveService = testerReactiveService;
    this.reportsComponent = reportsComponent;
  }

  onSubmitBook(): void {
    console.log('phone id: ' + this.selectedPhone + '\n' + 'tester id: ' + this.selectedTester);
    let observable = this.bookingReactiveService.book(this.selectedTester, this.selectedPhone);
    observable.subscribe(
      () => this.requestFreePhones(),
      (err) => console.error(err),
      () => this.reportsComponent.requestReportStream()
    );
  }

  onSubmitReturn(): void {
    let observable = this.bookingReactiveService.returnPhone(this.phoneToReturnId);
    observable.subscribe(
      () => this.requestFreePhones(),
      (err) => console.error(err),
      () => this.reportsComponent.requestReportStream()
    );
  }

  requestFreePhones(): void {
    let phoneObservable: Observable<Phone>;
    phoneObservable = this.phoneReactiveService.freePhones();
    this.phoneArray.clear();
    phoneObservable.subscribe(phone => {
      this.phoneArray.add(phone);
      this.cdr.detectChanges();
    });
    this.selectedPhone = '';
    this.selectedTester = '';
  }

  requestTesterStream(): void {
    let testerObservable: Observable<Tester>;
    testerObservable = this.testerReactiveService.getTesterStream();
    testerObservable.subscribe(tester => {
      this.testerArray.push(tester);
      this.cdr.detectChanges();
    });
    this.selectedPhone = '';
    this.selectedTester = '';
  }

  ngOnInit(): void {
   this.requestFreePhones();
   this.requestTesterStream();
  }
}
