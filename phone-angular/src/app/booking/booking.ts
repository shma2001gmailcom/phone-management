export class Booking {
  id: string;
  bookingTime: string;
  testerId: string;
  phoneId: string;

  constructor(id: string, bookingTime: string, testerId: string, phoneId: string) {
    this.id = id;
    this.bookingTime = bookingTime;
    this.testerId = testerId;
    this.phoneId = phoneId;
  }
}
