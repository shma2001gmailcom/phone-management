import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { Booking } from './booking';
import { ReportReactiveService } from '../report/report-reactive.service';

@Injectable()
export class BookingReactiveService {
  url: string = 'http://localhost:9001/v1/book';
  reportReactiveService: ReportReactiveService;
  constructor(private http: HttpClient, reportReactiveService: ReportReactiveService) {
    this.reportReactiveService = reportReactiveService;
  }

  book(testerId: string, phoneId: string): Observable<any> {
      const booking = new Booking(null, null, testerId, phoneId);
      console.log('booking: ' + booking);
      return this.http.post<Booking>(
        'http://localhost:9001/v1/book',
        booking,
        { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }
      );
  }

  returnPhone(phoneToReturnId: string): Observable<any> {
    return this.http.get<any>('http://localhost:9001/v1/unBook/' + phoneToReturnId);
  }
}
