import { Component, Input } from '@angular/core';
import { Booking } from './booking';

@Component({
  selector: 'app-booking-detail',
  templateUrl: './booking-detail.component.html'
})
export class BookingDetailComponent {
  @Input() booking: Booking;
}
