import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Report } from './report';

@Injectable()
export class ReportReactiveService {
  url: string = 'http://localhost:9001/v1/report';

  getReportStream(): Observable<Report> {
    return new Observable<Report>((observer) => {
      let url = this.url;
      let eventSource = new EventSource(url);
      eventSource.onmessage = (event) => {
        console.log('Received event: ', event.data);
        const json = JSON.parse(event.data);
        console.log('json: ', json);
        const when = json['when'];
        const time = when ? new Date(when) : null;
        const date = time ? time.toString() : '';
        observer.next(new Report(json['id'], json['phone'], date, json['who'], json['availability']));
      };
      eventSource.onerror = (error) => {
        if(eventSource.readyState === 0) {
          console.log('The stream has been closed by the server. \n' + error);
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }
}
