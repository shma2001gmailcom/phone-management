import { Component, Input } from '@angular/core';
import { Report } from './report';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html'
})
export class ReportDetailComponent {
  @Input() report: Report;
}
