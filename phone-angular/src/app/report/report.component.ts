import { Component, Input } from '@angular/core';
import { Report } from './report';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html'
})
export class ReportComponent {
  @Input() report: Report;
}
