export class Report {
  id: string;
  phone: string;
  when: string;
  who: string;
  availability: string;

  constructor(id: string, phone: string, when: string, who: string, availability: string) {
    this.id = id;
    this.phone = phone;
    this.when = when;
    this.who = who;
    this.availability = availability;
  }
}
