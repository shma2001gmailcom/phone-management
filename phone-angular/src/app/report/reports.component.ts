import { Report } from './report';
import { ReportReactiveService } from './report-reactive.service';

import { Observable } from 'rxjs';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";

@Component({
  selector: 'app-component-reports',
  providers: [ReportReactiveService],
  templateUrl: './reports.component.html'
})
export class ReportsComponent implements OnInit {
  reportArray: Report[] = [];
  selectedReport: Report;

  constructor(private reportReactiveService: ReportReactiveService, private cdr: ChangeDetectorRef) {}

  resetData() {
    this.reportArray = [];
  }

  requestReportStream(): void {
    this.resetData();
    let reportObservable: Observable<Report>;
    reportObservable = this.reportReactiveService.getReportStream();
    reportObservable.subscribe(report => {
      this.reportArray.push(report);
      this.cdr.detectChanges();
    });
  }

  onSelect(report: Report): void {
    this.selectedReport = report;
    this.cdr.detectChanges();
  }

  ngOnInit() {
    this.requestReportStream();
  }
}
