import { Component, Input } from '@angular/core';
import { Phone } from './phone';

@Component({
  selector: 'app-phone-detail',
  templateUrl: './phone-detail.component.html'
})
export class PhoneDetailComponent {
  @Input() phone: Phone;
}
