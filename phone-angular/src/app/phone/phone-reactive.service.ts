import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Phone } from './phone';

@Injectable()
export class PhoneReactiveService {

  url: string = 'http://localhost:9001/v1/phones/all';

  getPhoneStream(): Observable<Phone> {
    return new Observable<Phone>((observer) => {
      let url = this.url;
      let eventSource = new EventSource(url);
      eventSource.onmessage = (event) => {
        console.log('Received event: ', event.data);
        let json = JSON.parse(event.data);
        console.log('json: ', json);
        observer.next(new Phone(json['id'], json['name']));
      };
      eventSource.onerror = (error) => {
        if(eventSource.readyState === 0) {
          console.log('The stream has been closed by the server. \n' + error);
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }

  freePhones(): Observable<Phone> {
    return new Observable<Phone>((observer) => {
      let url = 'http://localhost:9001/v1/phone/free';
      let eventSource = new EventSource(url);
      eventSource.onmessage = (event) => {
        console.log('Received event: ', event.data);
        let json = JSON.parse(event.data);
        console.log('json: ', json);
        observer.next(new Phone(json['id'], json['name']));
      };
      eventSource.onerror = (error) => {
        if(eventSource.readyState === 0) {
          console.log('The stream has been closed by the server. \n' + error);
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }
}
