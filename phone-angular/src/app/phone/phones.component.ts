import { Phone } from './phone';
import { PhoneReactiveService } from './phone-reactive.service';

import { Observable } from 'rxjs';
import { ChangeDetectorRef, Component } from "@angular/core";

@Component({
  selector: 'app-component-phones',
  providers: [PhoneReactiveService],
  templateUrl: './phones.component.html'
})
export class PhonesComponent {
  phoneArray: Phone[] = [];
  selectedPhone: Phone;

  constructor(private phoneReactiveService: PhoneReactiveService, private cdr: ChangeDetectorRef) {}

  resetData() {
    this.phoneArray = [];
  }

  requestPhoneStream(): void {
    this.resetData();
    let phoneObservable: Observable<Phone>;
    phoneObservable = this.phoneReactiveService.getPhoneStream();
    phoneObservable.subscribe(phone => {
      this.phoneArray.push(phone);
      this.cdr.detectChanges();
    });
  }

  onSelect(phone: Phone): void {
    this.selectedPhone = phone;
    this.cdr.detectChanges();
  }

  ngOnInit() {
      this.requestPhoneStream();
  }
}
