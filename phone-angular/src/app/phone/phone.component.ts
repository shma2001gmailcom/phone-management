import { Component, Input } from '@angular/core';
import { Phone } from './phone';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.component.html'
})
export class PhoneComponent {
  @Input() phone: Phone;
}
