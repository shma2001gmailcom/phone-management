import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './http-error.interceptor';

import { AppComponent } from './app.component';
import { PhoneReactiveService } from './phone/phone-reactive.service';
import { PhonesComponent } from './phone/phones.component';
import { PhoneDetailComponent } from './phone/phone-detail.component';
import { TesterReactiveService } from './tester/tester-reactive.service';
import { TestersComponent } from './tester/testers.component';
import { TesterDetailComponent } from './tester/tester-detail.component';
import { ReportReactiveService } from './report/report-reactive.service';
import { ReportsComponent } from './report/reports.component';
import { ReportDetailComponent } from './report/report-detail.component';
import { BookingComponent } from './booking/booking.component';
import { BookingDetailComponent } from './booking/booking-detail.component';
import { BookingReactiveService } from './booking/booking-reactive.service';

@NgModule({
  declarations: [
    AppComponent,
    PhonesComponent,
    PhoneDetailComponent,
    TestersComponent,
    TesterDetailComponent,
    ReportsComponent,
    ReportDetailComponent,
    BookingComponent,
    BookingDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    PhoneReactiveService,
    TesterReactiveService,
    ReportReactiveService,
    BookingReactiveService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
