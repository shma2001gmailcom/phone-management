import { Component, Input } from '@angular/core';
import { Tester } from './tester';

@Component({
  selector: 'app-tester',
  templateUrl: './tester.component.html'
})
export class TesterComponent {
  @Input() tester: Tester;
}
