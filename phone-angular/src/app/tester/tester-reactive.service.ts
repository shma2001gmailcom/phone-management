import { Injectable } from '@angular/core';

import { Tester } from './tester';

import {Observable} from 'rxjs';

@Injectable()
export class TesterReactiveService {

  url: string = 'http://localhost:9001/v1/testers/all';


  getTesterStream(): Observable<Tester> {
    return new Observable<Tester>((observer) => {
      let url = this.url;
      let eventSource = new EventSource(url);
      eventSource.onmessage = (event) => {
        console.log('Received event: ', event.data);
        let json = JSON.parse(event.data);
        console.log('json: ', json);
        observer.next(new Tester(json['id'], json['name']));
      };
      eventSource.onerror = (error) => {
        if(eventSource.readyState === 0) {
          console.log('The stream has been closed by the server. \n' + error);
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }
}
