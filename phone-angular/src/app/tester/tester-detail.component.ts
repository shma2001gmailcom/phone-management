import { Component, Input } from '@angular/core';
import { Tester } from './tester';

@Component({
  selector: 'app-tester-detail',
  templateUrl: './tester-detail.component.html'
})
export class TesterDetailComponent {
  @Input() tester: Tester;
}
