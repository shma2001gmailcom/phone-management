import { Tester } from './tester';
import { TesterReactiveService } from './tester-reactive.service';

import { Observable } from 'rxjs';
import { ChangeDetectorRef, Component } from "@angular/core";

@Component({
  selector: 'app-component-testers',
  providers: [TesterReactiveService],
  templateUrl: './testers.component.html'
})
export class TestersComponent {
  testerArray: Tester[] = [];
  selectedTester: Tester;

  constructor(private testerReactiveService: TesterReactiveService, private cdr: ChangeDetectorRef) {}

  resetData() {
    this.testerArray = [];
  }

  requestTesterStream(): void {
    this.resetData();
    let testerObservable: Observable<Tester>;
    testerObservable = this.testerReactiveService.getTesterStream();
    testerObservable.subscribe(tester => {
      this.testerArray.push(tester);
      this.cdr.detectChanges();
    });
  }

  onSelect(tester: Tester): void {
    this.selectedTester = tester;
    this.cdr.detectChanges();
  }

  ngOnInit() {
    this.requestTesterStream();
  }
}
