package org.misha.phones.entity;

import jakarta.persistence.*;
import org.misha.phones.data.PhoneRecord;

import java.util.UUID;

@Entity
@Table(name = "PHONE")
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private UUID id;
    @Column(name = "name")
    private String name;

    public Phone() {
    }

    public Phone(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Phone of(PhoneRecord record) {
        return new Phone(record.id(), record.name());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PhoneRecord asRecord() {
        return new PhoneRecord(id, name);
    }
}
