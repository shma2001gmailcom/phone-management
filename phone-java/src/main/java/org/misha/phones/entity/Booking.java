package org.misha.phones.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.misha.phones.data.BookingRecord;

import java.sql.Timestamp;
import java.util.UUID;

import static java.time.LocalDateTime.now;

@Entity
@Table(name = "booking")
public class Booking {
    @Id
    @Column(name = "id")
    private UUID id;
    @Column(name = "booking_time")
    private Timestamp bookingTime;
    @Column(name = "tester_id")
    private UUID testerId;
    @Column(name = "phone_id")
    private UUID phoneId;

    public Booking() {
    }

    /**
     * The unique identifier of a booking coincides with the one of the correspondent phone, which is currently is
     * booking. So, any attempt to book the phone which has been already booked will end with constraint violation
     * exception until first booking isn't removed.
     */
    private Booking(UUID testerId, UUID phoneId) {
        this.id = phoneId;
        this.bookingTime = Timestamp.valueOf(now());
        this.testerId = testerId;
        this.phoneId = phoneId;
    }

    public static Booking of(UUID testerId, UUID phoneId) {
        return new Booking(testerId, phoneId);
    }

    public static Booking of(BookingRecord record) {
        return Booking.of(record.testerId(), record.phoneId());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getTesterId() {
        return testerId;
    }

    public void setTesterId(UUID testerId) {
        this.testerId = testerId;
    }

    public UUID getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(UUID phoneId) {
        this.phoneId = phoneId;
    }

    public Timestamp getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(Timestamp bookingTime) {
        this.bookingTime = bookingTime;
    }
}
