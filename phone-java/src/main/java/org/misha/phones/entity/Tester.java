package org.misha.phones.entity;

import jakarta.persistence.*;
import org.misha.phones.data.TesterRecord;

import java.util.UUID;

@Entity
@Table(name = "TESTER")
public class Tester {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private UUID id;
    @Column(name = "name")
    private String name;

    public Tester() {
    }

    public Tester(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Tester of(TesterRecord record) {
        return new Tester(record.id(), record.name());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TesterRecord asRecord() {
        return new TesterRecord(id, name);
    }
}
