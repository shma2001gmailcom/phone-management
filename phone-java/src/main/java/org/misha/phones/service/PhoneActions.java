package org.misha.phones.service;

import org.misha.phones.data.PhoneRecord;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

public interface PhoneActions {
    @Transactional
    Flux<PhoneRecord> getAllPhones();
}
