package org.misha.phones.service;

import org.misha.phones.BookingException;
import org.misha.phones.data.BookingRecord;
import org.misha.phones.data.PhoneRecord;
import org.misha.phones.data.ReportRecord;
import org.misha.phones.repo.BookingRepo;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.springframework.transaction.annotation.Isolation.SERIALIZABLE;

@SuppressWarnings("unused")
@Component
class BookingService implements BookingActions {
    private final BookingRepo bookingRepo;

    public BookingService(BookingRepo bookingRepo) {
        this.bookingRepo = bookingRepo;
    }

    @Transactional
    public Flux<BookingRecord> getAllBookings() {
        try {
            return Flux.fromStream(bookingRepo.findAll().stream().map(BookingRecord::from));
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Transactional
    public Flux<PhoneRecord> freePhones() {
        try {
            return Flux.fromStream(bookingRepo.getFreePhones().stream());
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    /**
     * As all tables supposed to be relative small, we are using higher isolation level instead of synchronisation.
     */
    @Override
    @Transactional(isolation = SERIALIZABLE)
    public Mono<Void> book(BookingRecord bookingRecord) {
        try {
            bookingRepo.newBooking(bookingRecord.testerId(), bookingRecord.phoneId());
            return Mono.empty();
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    /**
     * As all tables supposed to be relative small, we are using higher isolation level instead of synchronisation.
     */
    @Override
    @Transactional(isolation = SERIALIZABLE)
    public Mono<Void> unBook(UUID bookingId) {
        try {
            bookingRepo.deleteById(bookingId);
            return Mono.empty();
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Override
    @Transactional
    public Flux<ReportRecord> report() {
        try {
            return Flux.fromStream(bookingRepo.report().stream());
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }
}
