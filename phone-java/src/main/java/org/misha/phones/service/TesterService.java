package org.misha.phones.service;

import org.misha.phones.BookingException;
import org.misha.phones.data.TesterRecord;
import org.misha.phones.entity.Tester;
import org.misha.phones.repo.TesterRepo;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SuppressWarnings("unused")
@Component
class TesterService implements TesterActions {
    private final TesterRepo testerRepo;

    public TesterService(TesterRepo testerRepo) {
        this.testerRepo = testerRepo;
    }

    @Override
    @Transactional
    public Flux<TesterRecord> getAll() {
        try {
            return Flux.fromStream(testerRepo.findAll().stream().map(Tester::asRecord));
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Transactional
    public Mono<TesterRecord> addTester(TesterRecord record) {
        try {
            testerRepo.save(Tester.of(record));
            return Mono.just(record);
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }
}
