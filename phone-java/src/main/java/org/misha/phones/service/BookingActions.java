package org.misha.phones.service;

import org.misha.phones.data.BookingRecord;
import org.misha.phones.data.PhoneRecord;
import org.misha.phones.data.ReportRecord;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.springframework.transaction.annotation.Isolation.SERIALIZABLE;

public interface BookingActions {
    @Transactional(isolation = SERIALIZABLE)
    Mono<Void> book(BookingRecord bookingRecord);

    @Transactional(isolation = SERIALIZABLE)
    Mono<Void> unBook(UUID bookingId);

    @Transactional
    Flux<ReportRecord> report();

    @Transactional
    Flux<PhoneRecord> freePhones();
}
