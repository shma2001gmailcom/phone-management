package org.misha.phones.service;

import org.misha.phones.data.TesterRecord;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

public interface TesterActions {
    @Transactional
    Flux<TesterRecord> getAll();
}
