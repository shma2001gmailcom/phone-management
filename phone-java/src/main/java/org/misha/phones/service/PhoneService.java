package org.misha.phones.service;

import org.misha.phones.BookingException;
import org.misha.phones.data.PhoneRecord;
import org.misha.phones.entity.Phone;
import org.misha.phones.repo.PhoneRepo;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SuppressWarnings("unused")
@Component
class PhoneService implements PhoneActions {
    private final PhoneRepo phoneRepo;

    public PhoneService(PhoneRepo phoneRepo) {
        this.phoneRepo = phoneRepo;
    }

    @Override
    @Transactional
    public Flux<PhoneRecord> getAllPhones() {
        try {
            return Flux.fromStream(phoneRepo.findAll().stream().map(Phone::asRecord));
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Transactional
    public Mono<PhoneRecord> addPhone(PhoneRecord record) {
        try {
            phoneRepo.save(Phone.of(record));
            return Mono.just(record);
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }
}


