package org.misha.phones.service;

import org.misha.phones.entity.Tester;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface AdminActions {
    @Transactional
    void fill();

    @Transactional
    boolean existsPhone(UUID testerId);

    @Transactional
    void removeAllBookings();

    @Transactional
    Tester getById(UUID testerId);

    @Transactional
    void removeAllPhones();

    @Transactional
    void removeAllTesters();

    @Transactional
    boolean existsTester(UUID testerId);
}
