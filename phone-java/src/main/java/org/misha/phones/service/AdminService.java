package org.misha.phones.service;

import org.misha.phones.BookingException;
import org.misha.phones.entity.Tester;
import org.misha.phones.repo.BookingRepo;
import org.misha.phones.repo.PhoneRepo;
import org.misha.phones.repo.TesterRepo;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@SuppressWarnings("unused")
@Component
class AdminService implements AdminActions {
    private final BookingRepo bookingRepo;
    private final PhoneRepo phoneRepo;
    private final TesterRepo testerRepo;

    public AdminService(BookingRepo bookingRepo, PhoneRepo phoneRepo, TesterRepo testerRepo) {
        this.bookingRepo = bookingRepo;
        this.phoneRepo = phoneRepo;
        this.testerRepo = testerRepo;
    }

    @Override
    public void fill() {
        try {
            bookingRepo.fill();
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Override
    public void removeAllBookings() {
        try {
            bookingRepo.deleteAll();
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Override
    public void removeAllPhones() {
        try {
            phoneRepo.deleteAll();
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Override
    public boolean existsPhone(UUID phoneId) {
        try {
            return phoneRepo.findById(phoneId).isPresent();
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Transactional
    public void removeAllTesters() {
        try {
            testerRepo.deleteAll();
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Override
    public boolean existsTester(UUID testerId) {
        try {
            return testerRepo.findById(testerId).isPresent();
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }

    @Override
    public Tester getById(UUID testerId) {
        try {
            Optional<Tester> value = testerRepo.findById(testerId);
            if (value.isPresent()) {
                return value.get();
            } else {
                throw new BookingException("not found");
            }
        } catch (Exception e) {
            throw new BookingException(e);
        }
    }
}
