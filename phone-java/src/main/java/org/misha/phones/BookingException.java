package org.misha.phones;

public class BookingException extends RuntimeException {

    public BookingException(String message) {
        super(message);
    }

    public BookingException(Throwable e) {
        super(e);
    }
}
