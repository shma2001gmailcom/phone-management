package org.misha.phones.repo;

import org.misha.phones.entity.Tester;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface TesterRepo extends JpaRepository<Tester, UUID> {
}
