package org.misha.phones.repo;

import org.misha.phones.data.PhoneRecord;
import org.misha.phones.data.ReportRecord;
import org.misha.phones.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.misha.utils.Mapper.mapBytes;

public interface BookingRepo extends JpaRepository<Booking, UUID> {

    static ReportRecord mapRecordRow(Object[] array) {
        UUID id = mapBytes((byte[]) array[0]);
        String phone = (String) array[1];
        Timestamp when = (Timestamp) array[2];
        String who = (String) array[3];
        String availability = (String) array[4];
        return new ReportRecord(id, phone, when, who, availability);
    }

    static PhoneRecord mapPhoneRow(Object[] array) {
        UUID id = mapBytes((byte[]) array[0]);
        String name = (String) array[1];
        return new PhoneRecord(id, name);
    }

    @Query(value =
            "select b.id, " +
                    "p.name phone, " +
                    "b.booking_time \"when\", " +
                    "(select name from tester where id = b.tester_id) \"who\", " +
                    "(select case when b.booking_time is null then 'YES' else 'NO' end) availability " +
                    "from booking b right join (select id, name from phone) p on b.id = p.id;",
            nativeQuery = true)
    List<Object[]> bookingInfo();

    default List<ReportRecord> report() {
        return bookingInfo().stream().map(BookingRepo::mapRecordRow).collect(Collectors.toList());
    }

    @Modifying
    @Query(value = "insert into booking(id, booking_time, tester_id, phone_id) " +
            "values(:phoneId, current_timestamp, :testerId, :phoneId);", nativeQuery = true)
    void newBooking(@Param("testerId") UUID testerId, @Param("phoneId") UUID phoneId);

    @Query(value = "select p.id, p.name from phone p left join booking b on (p.id = b.id) where b.id is null;",
            nativeQuery = true)
    List<Object[]> freePhones();

    default List<PhoneRecord> getFreePhones() {
        List<PhoneRecord> list = freePhones().stream().map(BookingRepo::mapPhoneRow).toList();
        System.out.println("\n\nfree phones:" + list + "\n\n");
        return list;
    }

    @Modifying
    @Query(value = "insert into tester values( RANDOM_UUID(), 'Pit'); " +
            "insert into tester values( RANDOM_UUID(), 'Jane'); " +
            "insert into tester values( RANDOM_UUID(), 'Bob'); " +
            "insert into tester values( RANDOM_UUID(), 'Ann'); " +
            "insert into tester values( RANDOM_UUID(), 'Sophy'); " +
            "insert into tester values( RANDOM_UUID(), 'Cate'); " +
            "insert into phone values(RANDOM_UUID(),'Samsung Galaxy S9'); " +
            "insert into phone values(RANDOM_UUID(),'Samsung Galaxy S8'); " +
            "insert into phone values(RANDOM_UUID(),'Samsung Galaxy S8'); " +
            "insert into phone values(RANDOM_UUID(),'Motorola Nexus 6'); " +
            "insert into phone values(RANDOM_UUID(),'Oneplus 9'); " +
            "insert into phone values(RANDOM_UUID(),'Apple iPhone 13'); " +
            "insert into phone values(RANDOM_UUID(),'Apple iPhone 12'); " +
            "insert into phone values(RANDOM_UUID(),'Apple iPhone 11'); " +
            "insert into phone values(RANDOM_UUID(),'iPhone X'); " +
            "insert into phone values(RANDOM_UUID(),'Nokia 3310');", nativeQuery = true)
    void fill();
}
