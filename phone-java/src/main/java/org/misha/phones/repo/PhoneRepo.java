package org.misha.phones.repo;

import org.misha.phones.entity.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PhoneRepo extends JpaRepository<Phone, UUID> {
}
