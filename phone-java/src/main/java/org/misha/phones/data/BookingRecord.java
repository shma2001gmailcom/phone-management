package org.misha.phones.data;

import com.fasterxml.jackson.core.type.TypeReference;
import org.misha.phones.BookingException;
import org.misha.phones.entity.Booking;
import org.misha.utils.Mapper;

import java.sql.Timestamp;
import java.util.UUID;

import static java.time.LocalDateTime.now;

public record BookingRecord(UUID id, Timestamp bookingTime, UUID testerId, UUID phoneId) {
    private static final TypeReference<BookingRecord> TYPE = new TypeReference<>() {
    };

    public static BookingRecord from(Booking booking) {
        return new BookingRecord(booking.getId(), booking.getBookingTime(), booking.getTesterId(), booking.getPhoneId());
    }

    public static BookingRecord from(String json) {
        BookingRecord newBooking = Mapper.read(json, TYPE);
        if (newBooking.bookingTime != null || newBooking.id != null) {
            throw new BookingException("Unable to book the phone");
        }
        return new BookingRecord(newBooking.phoneId, Timestamp.valueOf(now()), newBooking.testerId, newBooking.phoneId);
    }

    @Override
    public String toString() {
        return Mapper.write(this);
    }
}
