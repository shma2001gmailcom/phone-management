package org.misha.phones.data;

import org.misha.utils.Mapper;

import java.sql.Timestamp;
import java.util.UUID;

public record ReportRecord(UUID id, String phone, Timestamp when, String who, String availability) {

    @Override
    public String toString() {
        return Mapper.write(this);
    }
}
