package org.misha.phones.data;

import org.misha.utils.Mapper;

import java.util.UUID;

public record TesterRecord(UUID id, String name) {

    @Override
    public String toString() {
        return Mapper.write(this);
    }
}
