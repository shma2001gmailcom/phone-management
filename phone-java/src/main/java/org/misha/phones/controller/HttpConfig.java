package org.misha.phones.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

import java.util.List;

@SuppressWarnings("unused")
@Configuration
public class HttpConfig {
    @Value("${cors.allowedOrigins}")
    private String allowedOrigins;

    @Bean
    CorsWebFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        List.of(parseOrigins(allowedOrigins)).forEach(origin -> {
            config.setAllowCredentials(true);
            config.addAllowedOrigin(origin);
            config.addAllowedHeader("*");
            config.addAllowedMethod("*");
        });
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return new CorsWebFilter(source);
    }

    private static String[] parseOrigins(String allowedOrigins) {
        return allowedOrigins.split(",");
    }
}
