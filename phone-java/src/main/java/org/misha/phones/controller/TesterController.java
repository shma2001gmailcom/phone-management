package org.misha.phones.controller;

import org.misha.phones.data.TesterRecord;
import org.misha.phones.service.TesterActions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

@SuppressWarnings("unused")
@RestController
public class TesterController {
    private final TesterActions service;

    public TesterController(TesterActions service) {
        this.service = service;
    }

    /**
     * Get all testers
     */
    @GetMapping(value = "/v1/testers/all", produces = TEXT_EVENT_STREAM_VALUE)
    public Flux<TesterRecord> testersAll() {
        return service.getAll();
    }
}
