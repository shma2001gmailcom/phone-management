package org.misha.phones.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.misha.phones.data.BookingRecord;
import org.misha.phones.data.PhoneRecord;
import org.misha.phones.data.ReportRecord;
import org.misha.phones.service.AdminActions;
import org.misha.phones.service.BookingActions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

@SuppressWarnings("unused")
@RestController
public class BookingController {
    private static final TypeReference<Map<String, String>> mapType = new TypeReference<>() {
    };
    private final BookingActions service;
    private final AdminActions adminActions;

    public BookingController(BookingActions service, AdminActions adminActions) {
        this.service = service;
        this.adminActions = adminActions;
    }

    /**
     * @return phones haven't been booked
     */
    @GetMapping(value = "/v1/phone/free")
    public Flux<PhoneRecord> fetchFreePhones() {
        return service.freePhones();
    }

    /**
     * Book a phone
     */
    @PostMapping(value = "/v1/book", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<?>> book(@RequestBody String bookingJson) {
        return service.book(BookingRecord.from(bookingJson)).map(val -> new ResponseEntity<>(OK));
    }

    /**
     * Delete phone booking
     */
    @GetMapping(value = "/v1/unBook/{id}")
    public Mono<ResponseEntity<?>> unBook(@PathVariable("id") UUID bookingId) {
        return service.unBook(bookingId).map(payload -> new ResponseEntity<>(OK));
    }

    @GetMapping(value = "/v1/report", produces = TEXT_EVENT_STREAM_VALUE)
    public Flux<ReportRecord> report() {
        return service.report();
    }

    @GetMapping(value = "/v1/fill")
    public void fillDataBase() {
        adminActions.fill();
    }

    @GetMapping(value = "/v1/bookings/remove")
    public void removeAll() {
        adminActions.removeAllBookings();
    }
}
