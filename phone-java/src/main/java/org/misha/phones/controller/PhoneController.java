package org.misha.phones.controller;

import org.misha.phones.data.PhoneRecord;
import org.misha.phones.service.PhoneActions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

@SuppressWarnings("unused")
@RestController
public class PhoneController {
    private static final Logger log = LoggerFactory.getLogger(PhoneController.class);
    private final PhoneActions service;

    public PhoneController(PhoneActions service) {
        this.service = service;
    }

    /**
     * Get all phones
     */
    @GetMapping(value = "/v1/phones/all", produces = TEXT_EVENT_STREAM_VALUE)
    public Flux<PhoneRecord> phonesAll() {
        return service.getAllPhones();
    }
}
