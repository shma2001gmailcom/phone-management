package org.misha.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.misha.phones.BookingException;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.UUID;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.util.StdDateFormat.DATE_FORMAT_STR_ISO8601;

public enum Mapper {
    @SuppressWarnings("unused")
    MAPPER;

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.setDateFormat(new SimpleDateFormat(DATE_FORMAT_STR_ISO8601))
                .configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static <T> T read(String json, TypeReference<T> type) {
        try {
            return mapper.readValue(json, type);
        } catch (JsonProcessingException e) {
            throw new BookingException(e);
        }
    }

    public static <T> String write(T object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new BookingException(e);
        }
    }

    public static UUID mapBytes(byte[] bytes) {
        if (bytes == null || bytes.length == 0) return null;
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        long high = byteBuffer.getLong();
        long low = byteBuffer.getLong();
        return new UUID(high, low);
    }
}
