package org.misha.utils;

import java.sql.SQLException;

/**
 * Interactive command line tool to access a database using JDBC.
 */
public class ShellFacade {

    public static void main(String[] args) throws SQLException {
        new org.h2.tools.Shell().runTool(args);
    }
}

