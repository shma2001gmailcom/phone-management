drop table booking;
drop table tester;
drop table phone;

create table if not exists tester(id UUID DEFAULT RANDOM_UUID() PRIMARY KEY, name varchar(100));
insert into tester values( RANDOM_UUID(), 'Pit');
insert into tester values( RANDOM_UUID(), 'Jane');
insert into tester values( RANDOM_UUID(), 'Bob');
insert into tester values( RANDOM_UUID(), 'Ann');
insert into tester values( RANDOM_UUID(), 'Sophy');
insert into tester values( RANDOM_UUID(), 'Cate');
select * from tester;

create table if not exists phone(id UUID DEFAULT RANDOM_UUID() PRIMARY KEY, name varchar(100));
insert into phone values(RANDOM_UUID(),'Samsung Galaxy S9');
insert into phone values(RANDOM_UUID(),'Samsung Galaxy S8');
insert into phone values(RANDOM_UUID(),'Samsung Galaxy S8');
insert into phone values(RANDOM_UUID(),'Motorola Nexus 6');
insert into phone values(RANDOM_UUID(),'Oneplus 9');
insert into phone values(RANDOM_UUID(),'Apple iPhone 13');
insert into phone values(RANDOM_UUID(),'Apple iPhone 12');
insert into phone values(RANDOM_UUID(),'Apple iPhone 11');
insert into phone values(RANDOM_UUID(),'iPhone X');
insert into phone values(RANDOM_UUID(),'Nokia 3310');
select * from phone;

create table if not exists booking(id UUID PRIMARY KEY,
                     booking_time timestamp,
                     tester_id UUID,
                     phone_id UUID,
    constraint fk_tester_id foreign key (tester_id) references tester(id),
    constraint fk_phone_id foreign key (phone_id) references phone(id));


select b.id "booking id",
       p.name "phone",
       b.booking_time "when",
       (select name from tester where id = b.tester_id) "who",
       (select case when b.booking_time is null then 'YES' else 'NO' end) "availability"
from booking b right join (select id, name from phone) p on b.id = p.id;

select t.tester_name,
       t.phone_name
    from
    (select tester.name tester_name,
            phone.name phone_name
        from booking
        join tester on (booking.tester_id = tester.id)
        join phone on (booking.phone_id = phone.id)
    ) t
group by tester_name, phone_name;

select p.id,
       p.name
from phone p
left join booking b
on (p.id = b.id) where b.id is null;


