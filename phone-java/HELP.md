# Getting Started

### The Task

The mobile software testing team has 10 mobile phones that it needs
to share for testing purposes

- Samsung Galaxy S9
- 2x Samsung Galaxy S8
- Motorola Nexus 6
- Oneplus 9
- Apple iPhone 13
- Apple iPhone 12
- Apple iPhone 11
- iPhone X
- Nokia 3310

• Please create an application that allows a phone to be booked / returned

• The following information should also be available for each phone

- Availability (Yes / No)
- When it was booked
- Who booked the phone

### Guides

- Set up JDK in the files `bash/build.sh` and `bash/run.sh`.
- Run `bash/build.sh`.
- When application up and running, open http://localhost:8082/ in your browser, this is H2 database web console
  **user=sa; password=sa**.
- If you decide to run the application from IDE, after application up and running you can get **H2 shell** by launching
  `utils.ShellFacade.main`.
- Inspect tables `tester, phone, booking`, pay attention to constraints.
- The folder `curl` contains some http requests (note, that creating or deleting testers or phones are not implemented
  in the http flow.)

#### Using docker,

- cd to `/bash` and run `build-run-docker.sh` and wait until application up;
- open http://localhost:4200/
- fetch testers, phones, reports
- book or return phones using angular application like it is shown on the picture below:
  ![browser-picture.png](browser-picture.png)

### Solution Explanation

The main mechanism to prevent repeated booking of the same phone consists of two parts:

1. Unique constraint on `phone_id` column of the `booking` table.
2. Insertion technique: when creating a new `booking`, the new `id` is provided for the `booking` which coincides to
   the `id` of the `phone` being booked (see `org.misha.phones.data.BookingRecord.from(java.lang.String)` and usage
   `org.misha.phones.controller.BookingController.book`).

- Here is a bunch of curl requests for testing application. So, for the **'book'** or **'return**' use
  `curl/curl-book.sh` or `curl/curl-return.sh` respectively. As for **'The following information should also be
  available
  for each phone'-** part, it is possible to check it either by running `curl/curl-report.sh` or directly, using last
  sql
  query at `resources/notepad.sql` from H2 console or H2 shell.
- Serialization approach is straight-forward and uses only jackson lib.

**Java application port**

- on local is **9000**;
- on the docker is **9001**, as it is pointed out in the `docker-compose.yml` file.

I decided also do not provide unit-tests. It sounds strange, but all the logic
is contained in unique index and filling data transfer objects special way.

It can only be integration and not unit tested.

The rest of code is just Spring Data JPA and Spring Http usage.

### Class Diagram

![phones.svg](phones.svg)

The picture of the result of the all phones request made in the browser:
![all-phones-docker.png](all-phones-docker.png)

Duplicated phone booking behaviour:
![duplicared-booking.png](duplicated-booking.png)
