#!/usr/bin/env bash
# ports:
# -local: 9000;
# -docker exposed: 9001
curl -i -vvv http://localhost:9001/v1/bookings/remove &&
  curl -i -vvv http://localhost:9001/v1/testers/remove &&
  curl -i -vvv http://localhost:9001/v1/phones/remove
