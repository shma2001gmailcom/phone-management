#!/usr/bin/env bash
# replace port with 9001 to check app health inside container
curl -i -vvv http://localhost:9001/actuator/health
echo
